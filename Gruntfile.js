

module.exports = function (grunt){
    requiere('time-grunt')(grunt);
    requiere('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    })
    grunt.initConfig({
        sass: {
            dist: {
                files:[{
                    expand:true,
                    cwd: 'css',
                    src:['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        whatch:{
            files:['css/*.scss'],
            tasks: ['css']
        },
        browserSync:{
            dev:{
                bsFiles:{
                    src:[
                        'css/*.css',
                        '*.html',
                        'js/*.js'  
                    ]
                },
                options:{
                    watchTask:true,
                    server:{
                        baseDir: './'
                    }
                }
            }
        },
        imagemin:{
            dynamic:{
                files:{
                    expand: true,
                    cwd: './',
                    src: 'img/*.{png,gif,jpg,jpeg}',
                    dest:'dist/'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('css', ['sass']);
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('grunt-browser-sync');
    grunt.registerTask('defult',['browserSync', 'watch']);
    grunt.registerTask('img:compress',['imagemin']);

    
};